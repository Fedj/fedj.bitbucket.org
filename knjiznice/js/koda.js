
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var trenutniEhr = "";

var pacienti = [
    {ime: 'Earl', priimek: 'Simmons', rojstvo: '1970-12-18'},
    {ime: "Miley", priimek: "Cyrus", rojstvo: "1992-11-23"},
    {ime: "Hillary", priimek: "Duff", rojstvo: "1987-09-28"}
    ];

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    trenutniEhr = ehrId;
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}



function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    var ehr = "";
  
    sessionId = getSessionId();

	var ime = pacienti[stPacienta].ime;
	var priimek = pacienti[stPacienta].priimek;
	var datumRojstva = pacienti[stPacienta].rojstvo;

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#preberiPredlogoBolnika").append("<option value="+ime+","+priimek+","+datumRojstva+">"+ime+" "+ priimek +"</option>");
		                    $("#preberiObstojeciEHR").append("<option value="+ehrId+">"+ime+" "+ priimek +"</option>");
		                    trenutniEhr = ehrId;
                            alert(stPacienta+1+". pacient: " +ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function zgeneriraj() {
    for (var i = 0; i < 3; i++) 
        generirajPodatke(i);
}

$(document).ready(function() {
   
   $("#preberiPredlogoBolnika").change(function() {
      $("#kreirajSporocilo").html("");
      var podatki = $(this).val().split(",");
      $("#kreirajIme").val(podatki[0]);
      $("#kreirajPriimek").val(podatki[1]);
      $("#kreirajDatumRojstva").val(podatki[2]); 
   });
   
   $("#preberiObstojeciEHR").change(function() {
       $("#preberiSporocilo").html("");
	   $("#preberiEHRid").val($(this).val());
   });
    
});

function dodajAlergijo() {
	sessionId = getSessionId();
	
	var ehrId = trenutniEhr;
	var alergija = $("#dodajAlergijo").val();
	if (!ehrId || !alergija || ehrId.trim().length == 0 || alergija.trim().length == 0)
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
     else {
     	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			"allergies/allergy": alergija
		};
		
		var parametriZahteve = {
			ehrId: ehrId,
			templateId: 'Allergies',
			format: 'FLAT'
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		
     }
}

function dodaj() {
	sessionId = getSessionId();

	$("#info").empty();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var teza = $("#kreirajTezo").val();
	var visina = $("#kreirajVisino").val();
	var dia = $("#kreirajDiastKrvni").val();
	var sist = $("#kreirajSistKrvni").val();
	var alergija = $("#dodajAlergijo").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    trenutniEhr = ehrId;
		                	var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "vital_signs/height_length/any_event/body_height_length": visina,
							    "vital_signs/body_weight/any_event/body_weight": teza,
							    "vital_signs/blood_pressure/any_event/systolic": sist,
							    "vital_signs/blood_pressure/any_event/diastolic": dia
							};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT'
							};
							var info = {
								"ctx/language": "en",
							    "ctx/territory": "SI",
								"allergies/adverse_reaction_-_allergy:1/substance_agent": alergija
							}
							var parametri = {
								ehrId: ehrId,
							    templateId: 'Allergies',
							    format: 'FLAT'
							}
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        $("#kreirajSporocilo").html(
					              "<span class='obvestilo label label-success fade-in'>" +
					              res.meta.href + ".</span>");
							    },
							    error: function(err) {
							    	$("#kreirajSporocilo").html(
					            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
					            JSON.parse(err.responseText).userMessage + "'!");
							    }
							});
							$.ajax({
								url: baseUrl + "/composition?" + $.param(parametri),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(info),
							    success: function (res) {
					              //alergiziraj(alergija);
					              if (alergija != '/' && alergija != ' ') {
					              	alergiziraj(alergija);
					              }
					         
							    },
							    error: function(err) {
							    	$("#kreirajSporocilo").html(
					            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
					            JSON.parse(err.responseText).userMessage + "'!");
							    }
							});
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		        
		    }
		});
	}
}

function preberiAlergije() {
	sessionId = getSessionId();
	
	var ehrId = $("#preberiEHRzaAlerg").val();
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	}
     else {
		$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/allergy",
		    type: 'GET',
		    headers: {
		        "Ehr-Session": sessionId
		    },
		    success: function (res) {
		        for(var i=0; i<res.length; i++){
		            $("#preberiAllergies").html("<span class='obvestilo label label-success fade-in'>" +
					              res[i].agent + ".</span>");
		            //zapiši v okno spodaj alergije.....
		        }
		    }
		});
     }
}

function alergiziraj(alergija) {
	if (alergija == "/") {
		$("#kreirajSporocilo").html("<div><br>Mogoče ste se zmotili, kajti ta stran je namenjena bolnikom z alergijami.</div>");
	}
	else {
		$("#kreirajSporocilo").html("<div><br>Prosim pomaknite se na dno strani, kjer piše kakšnih prehranjevalnih navad se naj bi držali, da dobite vse kar rabite.</div>");
		if (alergija == "Lactose intolerance") {
			$("#hrana").show();
			$("#pica").show();
		}
	}
	
	var url = "https://en.wikipedia.org/w/api.php?action=opensearch&search="+alergija+"&format=json&callback=?"
	
	$.ajax({
		type: 'GET',
		url: url,
		contentType: "application/json; charset=utf-8",
		async: false,
		dataType: "json",
		success: function(data) {
			console.log(data);
			$("#wiki").empty();
			$("#wiki").append(data[2]);
		},
		error: function (xhr, ajaxOptions, thrownError) {
    		alert(xhr.status);
    		alert(thrownError);
    	}
	});
	
}

$(document).ready(function() {
	$("#hrana").click(function(){
		$("#info").html("<div>Mlečni izdelki nudijo veliko beljakovin, so skoraj nekaj<br> najbolj običajnega v našem hladilniku.<br>Ker so tu ključne beljakovine priporočamo, da te pridobite s hranami<br> kot so ribe, belo meso in pa razni oreščki.<br>Priporočljivo je, da pred uporabo raznih živil pogledate<br> sestavine, saj se lahko nahajajo sledovi laktoze ali celo laktoza sama.</div>");
	});
	$("#pica").click(function(){
		$("#info").empty();
		$("#info").html("<div>Kot nadomestek mleku lahko pijete razna riževa in sojina mleka.");
	});
	
});

